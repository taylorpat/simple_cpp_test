# Simple unit testing framework

To use:
1. Clone this repository, once per assignment
2. Put your header files in the main directory with unit_wrapper.sh
3. Put your unit tests (1 per main/file) in the unit tests directory.
4. Run ./unit_wrapper.sh

voiala! 

Note: timer.sh can be edited to do accurate time-testing if you would like; this feature is also built into the unit_wrapper.sh.
