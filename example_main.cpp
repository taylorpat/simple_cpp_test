#include <iostream>
#include <utility>
#include "example_header.h"


int main()
{
    
    MyClass class_obj;
    
    std::string input_var;
    getline(std::cin, input_var);
    std::cout << input_var << std::endl;

    std::cout << "=== starting your main output. ===" << std::endl;

    std:: cout << "hypothetical extra junk you left in for testing...." << std::endl;

    if(class_obj.my_func() == 5)
    {
        std::cout << "five" << std::endl;
    }

    std::cout << "your cpp output ends here" << std::endl;

    return 0;
}

