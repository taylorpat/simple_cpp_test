/* Template for each unit test.
 * Make copies of this file, once for each function you want to test.
 * Keep those copies in the unit_tests/ folder here.
 * Use one *_test.cpp per function!
 */

#include <iostream>
#include <utility>

// Note: keep headers up one directory from tests, 
// in the same directory as the unit_wrapper.sh
//#include "../YourHeaderToTest.h"

int main()
{
    bool pass_test = false;
    try
    {
        // Tests of your code here
        // If code produces expected answers, set pass_test to true
    }
    // If you need to test the use of a specific exception type
    /*catch(const std::out_of_range &e)
    {
        if(e.what() == "whatever you want")
            pass_test = true;
    }*/ 
    catch(...)
    {
       // Your test crashed
       pass_test = false;
    }

    pass_test ? std::cout << " pass" : std::cout << " fail";
    
    return 0;
}

