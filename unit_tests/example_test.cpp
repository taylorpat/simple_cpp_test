// This is a functioning example of testing MyClass.my_func()

#include <iostream>
#include <utility>

// Note: keep headers up one directory from tests
#include "../example_header.h"

int main()
{
    bool pass_test = false;
    try
    {
        // Tests of your code here
        // If code produces expected answers, set pass_test to true
        MyClass class_obj;

        if(class_obj.my_func() == 5)
        {
            pass_test = true;
        }
    }
    // If you need to test the use of a specific exception type
    /*catch(const std::out_of_range &e)
    {
        if(e.what() == "whatever you want")
            pass_test = true;
    }*/ 
    catch(...)
    {
       // Your test crashed
       pass_test = false;
    }

    pass_test ? std::cout << " pass" : std::cout << " fail";
    
    return 0;
}

