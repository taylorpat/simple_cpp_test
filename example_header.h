#ifndef EXAMPLE_HEADER_H
#define EXAMPLE_HEADER_H

class MyClass
{
    public:
        // The purpose of this function is to return 5...
        int my_func()
        {
            return 5;
        }
};

#endif

