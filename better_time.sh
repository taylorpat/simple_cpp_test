#!/usr/bin/env bash

# usage:
# ./timer.sh "./yourprogram"

# ./timer.sh builtincommand

# Example:
# ./timer.sh ls

n=0
time_diff=0

for run in {1..100}
do
    n=$(( n + 1 ))
    t0=$(date +%s.%N)
    $1 >/dev/null 2>&1 
    t1=$(date +%s.%N)
    time_diff=$( echo "$time_diff + ( $t1 - $t0 )" | bc )
done
echo "Averaged across $n runs, your program's mean runtime is:
$( echo "$time_diff / $n" | bc -l )"

