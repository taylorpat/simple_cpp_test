#!/usr/bin/env bash
# Author: taylor@mst.edu
# Run this file, to test a directory of cpp files (unit_tests/)
# where each cpp file is a unit test for a header(s) 
# which reside inin the same directory as this file (the parent directory of unit_tests/)

subsectionfunc()
{
    echo -e '-------------------------------------------------------------------'
}

sectionfunc()
{
    echo -e '\n==================================================================='
}

###################################################################

clear
clear

###################################################################

sectionfunc
echo START UNIT TESTS:

for unit_test in unit_tests/*.cpp 
do
    echo Testing: "$unit_test"
    g++ -std=c++11 "$unit_test"
    ./a.out
    rm a.out
    echo
    subsectionfunc
done

sectionfunc

###################################################################

echo START MAIN DIFF CHECK

g++ -std=c++11 example_main.cpp
./a.out <sample_input.txt >your_output.txt
echo -e 'Difference between your output and sample output is:\n'
diff -u --color sample_output.txt your_output.txt
rm your_output.txt
rm a.out

sectionfunc

###################################################################

echo -e 'START TIME CHECK'

g++ -std=c++11 example_main.cpp

n=0
time_diff=0

for run in {1..100}
do
    n=$(( n + 1 ))
    t0=$(date +%s.%N)
    ./a.out <sample_input.txt >/dev/null 2>&1 
    t1=$(date +%s.%N)
    time_diff=$( echo "$time_diff + ( $t1 - $t0 )" | bc )
done
echo -e "Mean time across $n runs for your program's runtime is:\\n $( echo "$time_diff / $n" | bc -l ) seconds"

rm a.out

sectionfunc

###################################################################


#Section made by: Pranal Madria
#Email: pmkf2@mst.edu
#USAGE: Place source files in the mem_test directory. For each of your source files in the mem_test directory, the script below will run once.
#OUTPUT: Number of bytes lost.
echo -e 'START MEMORY LEAK TESTS'

for mem_test in mem_test/*.cpp
do
    echo -e Testing: "$mem_test"
    g++ -std=c++11 "$mem_test"
   
    #Pipes valgrind output into an output file. This file will be used later to be processed and read through looking for 'definitely lost'.
    valgrind ./a.out > log.txt 2>&1
    stringLost="definitely lost: "
    checkDefLost=$(grep -c "$stringLost" log.txt)
    
    #Below does the checking for if it even finds 'definitely lost'. If 'definitely lost' is not found, then there are definitely no memory leaks so it tags it as 0 bytes lost.
    if [[ $checkDefLost -eq 0 ]]
    then
        #Didn't find definitely lost therefore no memory leaks.
        holdAns=0
    else
        #This is where the reading of the number after definitely lost occurs.
        holdAns=$(awk '{if(/definitely lost: /) print $4}' < log.txt)
    fi
    
    #Check if the number of bytes is equal to zero and execute.
    if [[ $holdAns -eq 0 ]]
    then
        echo -e '\t Test passed, no leaks found!'
    else
        #Any number that is not equal to zero, therefore meaning that a certain amount of bytes have been lost.
        echo -e "\\t Test failed, memory leaks found with $holdAns bytes lost."
    fi

    rm log.txt
    rm a.out
    subsectionfunc
done

rm vgcore.* >/dev/null 2>&1

